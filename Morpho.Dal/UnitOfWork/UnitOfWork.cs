﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Morpho.Dal.Repository;
using Morpho.Dal.Context;
using System.Data.Entity;

namespace Morpho.Dal.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private DbContext _context;
        private DbContextTransaction transaction;
        private bool _disposed;

        public UnitOfWork()
        {
            _context = new NorthwindModel();
            _disposed = false;
        }
    
        public void RollBack()
        {
            transaction.Rollback();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                this._disposed = true;
            }
        }

        public void BeginTransaction()
        {
            transaction = _context.Database.BeginTransaction();
        }

        public void Commit()
        {
            transaction.Commit();
        }
    }
}
