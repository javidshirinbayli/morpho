﻿using Morpho.Dal.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Dal.UnitOfWork
{
    public interface IUnitOfWork
    {
        void BeginTransaction();
        int SaveChanges();
        void RollBack();
        void Commit();
        void Dispose();        
        
    }
}
