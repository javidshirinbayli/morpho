﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Morpho.Entities.DBModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Dal.Context
{
    public class MorphoDbContext : IdentityDbContext<>
    {
        public MorphoDbContext() :
            base("name=MorphoDbContext")
        {
        }
    }
}
