﻿using System;

namespace Morpho.Bl.FeedBack
{
    public abstract class Operations<T>
    {
        public T inputs { get; set; }
        public void Execute(T input)
        {
            inputs = input;
            //validate
            Run();
        }

        public abstract T Run();        
    }
}