﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Bl.FeedBack
{
    public sealed class CreateFeedBackParameters
    {
        public int TypeId { get; set; }
        public int StatusId { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }
}
