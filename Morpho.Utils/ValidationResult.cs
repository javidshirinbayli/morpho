﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Utils
{
    public class ValidationResult
    {
        public ValidationStatus Status { get; set; }
        public string Message { get; set; }

        public static ValidationResult CreateSuccessResult()
        {
            return new ValidationResult()
            {
                Status = ValidationStatus.SUCCESS,
            };
        }

        public static ValidationResult CreateErrorResult(string message)
        {
            return new ValidationResult()
            {
                Status = ValidationStatus.ERROR,
                Message = message
            };
        }
    }
}
