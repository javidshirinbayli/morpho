﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Utils
{
    public enum ValidationStatus
    {
        SUCCESS = 0,
        ERROR = 1
    }
}
