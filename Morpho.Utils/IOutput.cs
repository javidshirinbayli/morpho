﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morpho.Utils
{
    public interface IOutput
    {
        ErrorCodes ErrorCode { get; set; }
        string ErrorMessage { get; set; }
    }
}
